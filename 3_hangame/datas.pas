unit datas;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.StorageBin, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDatasGame = class(TDataModule)
    FDMemTable1: TFDMemTable;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FFileName: string;
    FRecordCount: Integer;
  public
    /// <summary>
    /// Get a random word
    /// </summary>
    function GetWord: string;
  end;

var
  DatasGame: TDatasGame;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

uses
  IOUtils;

procedure TDatasGame.DataModuleCreate(Sender: TObject);
begin
  if (FDMemTable1.Active) then
    FDMemTable1.Close;

  FFileName := TPath.Combine(TPath.GetDocumentsPath, 'dico_hangman.bin');

  if (not TFile.Exists(FFileName)) then
    raise Exception.Create('No datas');

  FDMemTable1.ResourceOptions.PersistentFileName := FFileName;
  FDMemTable1.ResourceOptions.Persistent := True;

  FDMemTable1.Open;
  FRecordCount := FDMemTable1.RecordCount;

  if (FRecordCount = 0) then
    raise Exception.Create('No datas');
end;

procedure TDatasGame.DataModuleDestroy(Sender: TObject);
begin
  if (FDMemTable1.Active) then
    FDMemTable1.Close;
end;

function TDatasGame.GetWord: string;
var
  idx: Integer;
begin
  Result := '';
  idx := Random(FRecordCount);

  FDMemTable1.First;
  while (not FDMemTable1.Eof) do
  begin
    Dec(idx);
    if (idx = 0) then
    begin
      Result := FDMemTable1.FieldByName('word').AsString;
      Break;
    end;
    FDMemTable1.Next;
  end;
end;

end.
