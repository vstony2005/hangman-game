unit frDrawHangman;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects;

type
  TFrDraw = class(TFrame)
    img1: TPath;
    img2: TPath;
    img6: TPath;
    img5: TPath;
    img3: TPath;
    img4: TPath;
    img7: TPath;
  private
    FId: Integer;
    FCurrent: TPath;
    FRatio: Extended;
  public
    constructor Create(AOwner: TComponent); override;
    /// <summary>
    ///   Hide all drawings
    /// </summary>
    procedure HideAll;
    /// <summary>
    ///   Display the next drawing
    /// </summary>
    function ShowNext: Boolean;
    /// <summary>
    ///   Ratio Height / Width
    /// </summary>
    property Ratio: Extended read FRatio;
  end;

implementation

{$R *.fmx}

{ TFrame1 }

constructor TFrDraw.Create(AOwner: TComponent);
begin
  inherited;
  FRatio := Height / Width;
end;

procedure TFrDraw.HideAll;
var
  i: Integer;
begin
  for i:=0 to ChildrenCount-1 do
  begin
    if (Children[i] is TPath) then
      (Children[i] as TPath).Visible := False;
  end;
  FId := 0;
  FCurrent := nil;
end;

function TFrDraw.ShowNext: Boolean;
var
  path: TPath;
begin
  Inc(FId);
  case FId of
    1: path := img1;
    2: path := img2;
    3: path := img3;
    4: path := img4;
    5: path := img5;
    6: path := img6;
    7: path := img7;
  else
    path := nil;
  end;

  if Assigned(FCurrent) then
    FCurrent.Visible := False;

  FCurrent := path;

  if Assigned(FCurrent) then
  begin
    FCurrent.Visible := True;
    Result := (FId < 7);
  end
  else
    Result := False;
end;

end.
