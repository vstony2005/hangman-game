unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts, uHangGame, frDrawHangman, FMX.Objects;

type
  TForm3 = class(TForm)
    LytButtons: TFlowLayout;
    Label1: TLabel;
    btnNewGame: TButton;
    Label2: TLabel;
    Layout1: TLayout;
    LytDraw: TLayout;
    FrDraw1: TFrDraw;
    procedure FormDestroy(Sender: TObject);
    procedure btnNewGameClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LytDrawResized(Sender: TObject);
  private
    FHangGame: THangGame;
    procedure BtnLetterClick(Sender: TObject);
    procedure SetWordToFind(Sender: TObject; const AWord: string);
    procedure SetScore(Sender: TObject; AScore: Integer);
    procedure ActiveButtons(IsActive: Boolean);
  public
  end;

var
  Form3: TForm3;

implementation

{$R *.fmx}

procedure TForm3.FormCreate(Sender: TObject);
var
  btn: TButton;
  c: Char;
begin
  FHangGame := THangGame.Create;
  FHangGame.OnDisplayedWordChange := SetWordToFind;
  FHangGame.OnScoreChange := SetScore;

  FrDraw1.HideAll;

  // create all buttons
  for c:='A' to 'Z' do
  begin
    btn := TButton.Create(LytButtons);
    btn.Name := c;
    btn.Parent := LytButtons;
    btn.Height := 44;
    btn.Width := 44;
    btn.Margins.Top := 5;
    btn.Margins.Right := 5;
    btn.Margins.Bottom := 5;
    btn.Margins.Left := 5;
    btn.OnClick := BtnLetterClick;
    btn.Tag := 12;
    btn.Enabled := False;
  end;

  Label1.Text := string.Empty;
  Label2.Text := string.Empty;
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  FHangGame.Free;
end;

procedure TForm3.SetScore(Sender: TObject; AScore: Integer);
begin
  Label2.Text := Format('Score : %d', [AScore]);
end;

procedure TForm3.SetWordToFind(Sender: TObject; const AWord: string);
begin
  Label1.Text := AWord;
end;

procedure TForm3.BtnLetterClick(Sender: TObject);
var
  c: Char;
  str, msgEnd: string;
  isEnd: Boolean;
begin
  if (Sender is TButton) and ((Sender as TButton).Tag = 12) then
  begin
    str := (Sender as TButton).Name;
    isEnd := False;

    if (FHangGame.AddLetter(str.Chars[0])) then
    begin
      if (FHangGame.IsWin) then
      begin
        isEnd := True;
        msgEnd := 'Win!';
      end;
    end
    else if (not FrDraw1.ShowNext) then
    begin
      isEnd := True;
      msgEnd := 'Loose!';
    end;

    if (isEnd) then
    begin
      Label1.Text := Format('%s [%s]',
                            [msgEnd,
                             FHangGame.Word]);
      ActiveButtons(False);
    end
    else
      (Sender as TButton).Enabled := False;
  end;
end;

procedure TForm3.btnNewGameClick(Sender: TObject);
begin
  FHangGame.Init;
  FrDraw1.HideAll;

  ActiveButtons(True);
end;

procedure TForm3.ActiveButtons(IsActive: Boolean);
var
  btn: TButton;
  i: Integer;
begin
  for i:=0 to LytButtons.ChildrenCount-1 do
  begin
    if (LytButtons.Children[i] is TButton) then
      (LytButtons.Children[i] as TButton).Enabled := IsActive;
  end;
end;

procedure TForm3.LytDrawResized(Sender: TObject);
begin
  if (FrDraw1.Ratio = 0) then Exit;

  if ((LytDraw.Height / LytDraw.Width) > 1) then
  begin
    FrDraw1.Width := LytDraw.Width;
    FrDraw1.Height := FrDraw1.Width * FrDraw1.Ratio;
  end
  else
  begin
    FrDraw1.Height := LytDraw.Height;
    FrDraw1.Width := FrDraw1.Height / FrDraw1.Ratio;
  end;
end;

end.
