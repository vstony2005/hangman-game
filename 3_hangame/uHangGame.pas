unit uHangGame;

interface

uses
  System.Classes;

type
  TScoreEvent = procedure(Sender: TObject; AScore: Integer) of object;
  TDisplayedEvent = procedure(Sender: TObject; const AWord: string) of object;

  THangGame = class
  private
    FOnDisplayedWordChange: TDisplayedEvent;
    FOnScoreChange: TScoreEvent;
    FWordDisplayed: string;
    FWordClean: string;
    FWordToFind: string;
    FScore: Integer;
    function GetIsWin: Boolean;
    function GetHiddenChar: Char;
    procedure SetScore(const Value: Integer);
    procedure SetWordClean(const Value: string);
    procedure SetWordDisplayed(const Value: string);
    procedure SetWordToFind(const Value: string);

    property Score: Integer read FScore write SetScore;
    property WordToFind: string read FWordToFind write SetWordToFind;
    property WordDisplayed: string read FWordDisplayed write SetWordDisplayed;
    property WordClean: string read FWordClean write SetWordClean;
  public
    /// <summary>
    ///   Initialize the game
    /// </summary>
    procedure Init;
    /// <summary>
    ///   Suggest a letter for the word to find
    /// </summary>
    function AddLetter(const ALetter: Char): Boolean;
  published
    /// <summary>
    ///   Function called when the displayed word changes
    /// </summary>
    property OnDisplayedWordChange: TDisplayedEvent read FOnDisplayedWordChange write
        FOnDisplayedWordChange;
    /// <summary>
    ///   Function called when the score changes
    /// </summary>
    property OnScoreChange: TScoreEvent read FOnScoreChange write FOnScoreChange;
    /// <summary>
    ///   Indicates if the game is won
    /// </summary>
    property IsWin: Boolean read GetIsWin;
    /// <summary>
    ///   Word to find
    /// </summary>
    property Word: string read FWordToFind;
  end;

implementation

uses
  datas, System.SysUtils;

{ THangGame }

procedure THangGame.Init;
begin
  Score := 0;
  WordToFind := DatasGame.GetWord;
end;

function THangGame.AddLetter(const ALetter: Char): Boolean;
var
  str: string;
  i: Integer;
  c1, c2: Char;
  nb: Integer;
begin
  str := WordDisplayed;
  nb := 0;

  for i:=0 to WordClean.Length-1 do
  begin
    c1 := WordDisplayed.Chars[i];
    c2 := WordClean.Chars[i];

    if (c1 = GetHiddenChar) and (c2 = ALetter) then
    begin
      str[i+1] := WordToFind.Chars[i];
      Inc(nb);
    end;
  end;

  Result := nb > 0;
  WordDisplayed := str;
  Score := Score + nb;
end;

function THangGame.GetIsWin: Boolean;
begin
  Result := FWordDisplayed.IndexOf(GetHiddenChar) < 0;
end;

function THangGame.GetHiddenChar: Char;
begin
  Result := '*';
end;

procedure THangGame.SetScore(const Value: Integer);
begin
  FScore := Value;

  if Assigned(OnScoreChange) then
    OnScoreChange(Self, Score);
end;

procedure THangGame.SetWordToFind(const Value: string);

  function CleanText(const str: string): string;
  const withoutAccents : array[#0..#255] of Char
               =  #0#1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16
                  + #17#18#19#20#21#22#23#24#25#26#27#28#29#30#31
                  + ' !"#$%&''()*+,-./0123456789:;<=>?'
                  + '@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_'
                  + '`abcdefghijklmnopqrstuvwxyz{|}~'#127
                  + '�'#129'��������S��'#141'�'#143#144'���������s��'#157'zY'
                  + #160'������������*������������������'
                  + 'AAAAAA�CEEEEIIIIDNOOOOO�OUUUUY��'
                  + 'aaaaaa�ceeeeiiiidnooooo�ouuuuy�y';
  var
    i: Integer;
  begin
    Result := str;
    for i := 1 to Length(Result) do
      Result[i] := withoutAccents[Result[i]] ;
  end;

begin
  FWordToFind := Value.ToUpper;
  WordClean := CleanText(WordToFind)
end;

procedure THangGame.SetWordClean(const Value: string);
var
  str: string;
  i: Integer;
  c: Char;
begin
  FWordClean := Value;

  str := string.empty;
  for i:=0 to Value.Length-1do
  begin
    c := Value.Chars[i];

    if CharInSet(c, ['A'..'Z']) then
      str := str + GetHiddenChar
    else
      str := str + c;
  end;

  WordDisplayed := str;
end;

procedure THangGame.SetWordDisplayed(const Value: string);
begin
  FWordDisplayed := Value;

  if Assigned(OnDisplayedWordChange) then
    OnDisplayedWordChange(Self, WordDisplayed);
end;

end.
