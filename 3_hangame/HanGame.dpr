program HanGame;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.StartUpCopy,
  FMX.Forms,
  fMain in 'fMain.pas' {Form3},
  datas in 'datas.pas' {DatasGame: TDataModule},
  uHangGame in 'uHangGame.pas',
  frDrawHangman in 'frDrawHangman.pas' {FrDraw: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDatasGame, DatasGame);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
