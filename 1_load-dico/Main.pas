unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Comp.BatchMove,
  FireDAC.Comp.BatchMove.Text, Data.Bind.Controls, Data.Bind.Components,
  Data.Bind.DBScope, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts,
  FMX.Bind.Navigator, FMX.ListBox, FireDAC.Comp.BatchMove.DataSet, System.Rtti,
  System.Bindings.Outputs, FMX.Bind.Editors, Data.Bind.EngExt,
  FMX.Bind.DBEngExt,
  FireDAC.UI.Intf, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, FMX.Grid.Style,
  FMX.ScrollBox, FMX.Grid, FMX.ExtCtrls, FMX.Bind.Grid, Data.Bind.Grid,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageBin;

type
  TForm2 = class(TForm)
    FDBatchMoveTextReader1: TFDBatchMoveTextReader;
    FDBatchMove1: TFDBatchMove;
    FDMemTable1: TFDMemTable;
    FDBatchMoveDataSetWriter1: TFDBatchMoveDataSetWriter;
    BindNavigator1: TBindNavigator;
    btnLoad: TButton;
    BindSourceDB1: TBindSourceDB;
    OpenDialog1: TOpenDialog;
    BindingsList1: TBindingsList;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    Grid1: TGrid;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    FDMemTable1word: TWideStringField;
    Label1: TLabel;
    Button1: TButton;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    procedure FormCreate(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FDBatchMove1WriteRecord(ASender: TObject;
      var AAction: TFDBatchMoveAction);
    procedure FormShow(Sender: TObject);
    procedure Label1Click(Sender: TObject);
  private
    FFileName: string;
    procedure ShowNbElements;
  public
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  System.Diagnostics, IOUtils;

procedure TForm2.FormCreate(Sender: TObject);
begin
  FDBatchMoveTextReader1.DataDef.RecordFormat :=  TFDTextRecordFormat.rfFieldPerLine;
  FDBatchMoveTextReader1.Encoding := TFDEncoding.ecUTF8;
  FDBatchMove1.LogFileEncoding := TFDEncoding.ecUTF8;

  if (FDMemTable1.Active) then
    FDMemTable1.Close;

  FFileName := TPath.Combine(TPath.GetDocumentsPath, 'dico_hangman.bin');
  FDMemTable1.ResourceOptions.Persistent := True;
  FDMemTable1.ResourceOptions.PersistentFileName := FFileName;

  FDMemTable1.Open;
end;

procedure TForm2.btnLoadClick(Sender: TObject);
var
  watch : TStopWatch;
  i: Integer;
begin
  if (OpenDialog1.Execute) then
  begin
    if FDMemTable1.Active then
      FDMemTable1.Close;


    watch := TStopwatch.Create;
    watch.Start;
    FDBatchMoveTextReader1.FileName := OpenDialog1.FileName;
    i := FDBatchMove1.Execute;
    FDMemTable1.Open;
    Label1.Text := Format('Batchmove total  %d en %d millisecondes',
                   [i,Watch.ElapsedMilliseconds]);
    watch.Stop;
    FDMemTable1.First;
  end;
end;

procedure TForm2.FDBatchMove1WriteRecord(ASender: TObject;
  var AAction: TFDBatchMoveAction);
var
  str: string;
  accept: Boolean;
begin
  AAction := TFDBatchMoveAction.paInsert;
  str := FDBatchMove1.Mappings[0].ItemValue;
  accept := (Length(str) >= 6) AND (Length(str) <= 12);

  if not accept then
    AAction := TFDBatchMoveAction.paSkip;
end;

procedure TForm2.Label1Click(Sender: TObject);
begin
  ShowNbElements;
end;

procedure TForm2.ShowNbElements;
begin
  Label1.Text := Format('%d elements', [FDMemTable1.RecordCount]);
end;

procedure TForm2.Button1Click(Sender: TObject);
var
  lst: TStringList;
begin
  if (FDMemTable1.RecordCount = 0) then
  begin
    Label1.Text := 'Nothing to save';
    Exit;
  end;

  FDMemTable1.DisableControls;
  FDMemTable1.First;

  lst := TStringList.Create;
  try
    while (not FDMemTable1.Eof) do
    begin
      lst.Add(FDMemTable1.FieldByName('word').AsString);
      FDMemTable1.Next;
    end;

    lst.SaveToFile('dico.txt');
  finally
    lst.Free;
  end;

  FDMemTable1.First;
  FDMemTable1.EnableControls;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  if (TFile.Exists(FFileName)) then
    Label1.Text := 'File not found'
  else
    ShowNbElements;
end;

end.
