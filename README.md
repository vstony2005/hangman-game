# Hangman Game

## LoadWordList

FMX Project / Delphi 10.4

Loads a word list from a text file into a binary file.

The data is saved in the "My Documents" folder.

## TestVersion

FMX Project / Delphi 10.4

Test project to set up the game.

## HanGame

FMX Project / Delphi 10.4

Use of a word list loaded from a binary file placed in the "My Documents" folder.

Implementation inspired by the following videos:

[Formation Delphi - jour 19](https://apprendre-delphi.fr/formation-delphi-20200421.php)

[Formation Delphi - jour 20](https://apprendre-delphi.fr/formation-delphi-20200422.php)
