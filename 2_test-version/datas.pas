unit datas;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin;

type
  THangDatas = class(TDataModule)
    FDMemTable1: TFDMemTable;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FFileName: string;
    FRecordCount: Integer;
  public
    function GetWord: string;
  end;

var
  MyDatas: THangDatas;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

uses
  IOUtils;

procedure THangDatas.DataModuleCreate(Sender: TObject);
begin
  if (FDMemTable1.Active) then
    FDMemTable1.Close;

  FFileName := TPath.Combine(TPath.GetDocumentsPath, 'dico_hangman.bin');
  FDMemTable1.ResourceOptions.Persistent := True;
  FDMemTable1.ResourceOptions.PersistentFileName := FFileName;

  FDMemTable1.Open;
  FRecordCount := FDMemTable1.RecordCount;
end;

procedure THangDatas.DataModuleDestroy(Sender: TObject);
begin
  if (FDMemTable1.Active) then
    FDMemTable1.Close;
end;

function THangDatas.GetWord: string;
var
  idx: Integer;
begin
  Result := '';

  idx := Random(FRecordCount);

  FDMemTable1.First;
  while (not FDMemTable1.Eof) do
  begin
    Dec(idx);
    if (idx = 0) then
    begin
      Result := FDMemTable1.FieldByName('word').AsString;
      Break;
    end;
    FDMemTable1.Next;
  end;
end;

end.
