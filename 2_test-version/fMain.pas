unit fMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    FWordDisplayed: string;
    FWordToFind: string;
    FWordClean: string;
    procedure SetWordClean(const Value: string);
    procedure SetWordDisplayed(const Value: string);
    procedure SetWordToFind(const Value: string);
  public
    /// <summary>
    ///   Word to find
    /// </summary>
    property WordToFind: string read FWordToFind write SetWordToFind;
    /// <summary>
    ///   Word displayed in game
    /// </summary>
    property WordDisplayed: string read FWordDisplayed write SetWordDisplayed;
    /// <summary>
    ///   Word without accents
    /// </summary>
    property WordClean: string read FWordClean write SetWordClean;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  datas;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Edit1.Text := 'A';
end;

procedure TForm1.SetWordClean(const Value: string);
var
  i: Integer;
  c: Char;
  str: string;
begin
  FWordClean := Value;

  str := string.empty;
  for i:=0 to Value.Length-1do
  begin
    c := Value.Chars[i];

    if CharInSet(c, ['A'..'Z']) then
      str := str + '*'
    else
      str := str + c;
  end;

  WordDisplayed := str;
end;

procedure TForm1.SetWordDisplayed(const Value: string);
begin
  FWordDisplayed := Value;
  Label1.Text := FWordDisplayed;
end;

procedure TForm1.SetWordToFind(const Value: string);

  function CleanText(const str: string): string;
  const withoutAccents : array[#0..#255] of Char
               =  #0#1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16
                  + #17#18#19#20#21#22#23#24#25#26#27#28#29#30#31
                  + ' !"#$%&''()*+,-./0123456789:;<=>?'
                  + '@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_'
                  + '`abcdefghijklmnopqrstuvwxyz{|}~'#127
                  + '�'#129'��������S��'#141'�'#143#144'���������s��'#157'zY'
                  + #160'������������*������������������'
                  + 'AAAAAA�CEEEEIIIIDNOOOOO�OUUUUY��'
                  + 'aaaaaa�ceeeeiiiidnooooo�ouuuuy�y';
  var
    i: Integer;
  begin
    Result := str;
    for i := 1 to Length(Result) do
      Result[i] := withoutAccents[Result[i]] ;
  end;

begin
  FWordToFind := Value.ToUpper;
  WordClean := CleanText(FWordToFind);
  Label2.Text := WordToFind;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  WordToFind := MyDatas.GetWord;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  c, c1, c2: Char;
  i: Integer;
  str: string;
begin
  if (Edit1.Text = string.Empty) then
    Exit;

  c := Edit1.Text.ToUpper.Chars[0];
  str := WordDisplayed;

  for i:=0 to FwordClean.Length-1 do
  begin
    c1 := WordDisplayed.Chars[i];
    c2 := WordClean.Chars[i];

    if (c1 = '*') and (c2 = c) then
      str[i+1] := WordToFind[i+1];
  end;

  WordDisplayed := str;
end;

end.
